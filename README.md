# ansible-role-woodpecker-server

This role has goal to install and configure an woodpecker-server (docker based)

It configure :  
* docker network
* docker container (woodpecker-agent)

## Setup environment 
```shell
sudo make install-python
sudo apt-get install direnv -y
if [ ! "$(grep -ir "direnv hook bash" ~/.bashrc)" ];then echo 'eval "$(direnv hook bash)"' >> ~/.bashrc; fi && direnv allow . && source ~/.bashrc
make env prepare
```

## Testing ansible-role-woodpecker-agent execution in vagrant-docker environment
```shell
make tests-docker-local
```

## Variables
Take a look at **roles/ansible-role-woodpecker-server/defaults/main.yml** file to have more informations about variables  

## To know
You must install and configure an Gitea instance if you want test this role.  

For the moment, this role only support Gitea as version control system.

Enjoy :)
